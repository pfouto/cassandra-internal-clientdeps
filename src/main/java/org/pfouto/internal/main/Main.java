package org.pfouto.internal.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.state.MessageExecutor;
import org.pfouto.internal.state.LocalMessageReceiver;
import org.pfouto.internal.tree.TreeManager;

import java.io.IOException;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);

    public static boolean NO_DEPS_TO_DISK = false;
    public static int REMOTE_WAIT_MS;

    public static void main(String[] args) throws IOException {

        TreeManager.getInstance().loadLayout(args[0]);

        REMOTE_WAIT_MS = Integer.valueOf(args[1]);

        if(args.length>2){
            NO_DEPS_TO_DISK = Boolean.valueOf(args[2]);
        }

        ConnectionManager.init();

        LocalMessageReceiver localReceiver = new LocalMessageReceiver();
        MessageExecutor migrateExecutor = new MessageExecutor(null, true);
        new Thread(localReceiver, "Local").start();
        new Thread(migrateExecutor, "Migrate").start();
    }
}
