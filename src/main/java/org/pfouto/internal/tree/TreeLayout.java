package org.pfouto.internal.tree;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class TreeLayout {
    Map<String, Datacenter> dcs;
    String mainTree;

    public class Datacenter{
        String ip;
        Map<String, List<String>> paths;
        List<String> keyspaces;
    }

}

