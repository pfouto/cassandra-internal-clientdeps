package org.pfouto.internal.tree;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.*;

public class TreeManager {

    private static final Logger logger = LogManager.getLogger(TreeManager.class);

    private static TreeManager treeManager;

    private TreeLayout layout;
    private String localDc;
    //          source      branch  dcs behind branch
    private Map<String, Map<String, Set<String>>> forwardTable;

    private Map<String, String> inversePaths;

    private TreeManager() {
    }

    public static synchronized TreeManager getInstance() {
        if (treeManager == null)
            treeManager = new TreeManager();
        return treeManager;
    }

    public String getLocalDc() {
        return localDc;
    }

    public void loadLayout(String path) throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader(path));
        layout = gson.fromJson(jsonReader, TreeLayout.class);

        StringBuilder sb = new StringBuilder();
        sb.append("Layout:\n");
        layout.dcs.forEach((k,v) -> sb.append(k).append(" ").append(v.ip).append(" ").append(v.paths).append('\n'));
        layout.dcs.forEach((k,v) -> { if (isMyIpAddress(v.ip)) localDc = k;});
        logger.info(sb.toString());
        if(localDc == null){
            logger.error("Couldn't determine localdc");
            System.exit(0);
        }
        logger.info("LocalDc: " + localDc);

        //setup forward table
        forwardTable = new HashMap<>();
        for(String source : layout.dcs.keySet()){
            Map<String, Set<String>> put = new HashMap<>();
            forwardTable.put(source, put);
            Map<String, List<String>> paths = layout.dcs.get(source).paths;
            for(String branch : getDownBranches(source)) {
                Set<String> list = new TreeSet<>();
                put.put(branch, list);
                for (String target : layout.dcs.keySet()) {
                    if(leadsTo(branch, target, paths)){
                        list.add(target);
                    }
                }
            }
        }

        for(Map.Entry<String, Map<String, Set<String>>> e : forwardTable.entrySet()){
            if(e.getValue().isEmpty())
                continue;
            logger.info(e.getKey());
            for (Map.Entry<String, Set<String>> e2 : e.getValue().entrySet()){
                logger.info("\t" + e2.getKey() + "\t" + e2.getValue());
            }
        }

        //setup inverse paths
        inversePaths = new HashMap<>();
        for(Map.Entry<String, TreeLayout.Datacenter> dc : layout.dcs.entrySet()){
            for(Map.Entry<String, List<String>> paths : dc.getValue().paths.entrySet()){
                for (String v : paths.getValue()) {
                    if(v.equals(localDc)){
                        inversePaths.put(dc.getKey(), paths.getKey());
                    }
                }
            }
        }
        System.out.println("Inverse paths: " + inversePaths);

        logger.info("Main tree: " + layout.mainTree);
    }

    public String getUpBranch(String tree) {
        return inversePaths.get(tree);
    }

    public String getMainTree(){
        return layout.mainTree;
    }

    public String getDcAddress(String dc){
        return layout.dcs.get(dc).ip;
    }

    public List<String> getDownBranches(String sourceDc){
        return layout.dcs.get(sourceDc).paths.getOrDefault(localDc, Collections.emptyList());
    }

    public Set<String> getTargetsBehindBranch(String sourceDc, String nextDc)
    {
        return new TreeSet<>(forwardTable.get(sourceDc).get(nextDc));
    }

    private boolean leadsTo(String startDc, String targetDc, Map<String, List<String>> paths){
        if(startDc.equals(targetDc))
            return true;

        if(!paths.containsKey(startDc))
            return false;

        for(String dc : paths.get(startDc)){
            if(leadsTo(dc, targetDc, paths)){
                return true;
            }
        }
        return false;
    }

    public String getDcNameByInternalAddress(InetAddress address) throws UnknownHostException {
        for(Map.Entry<String, TreeLayout.Datacenter> e : layout.dcs.entrySet()){
            if(InetAddress.getByName(e.getValue().ip).equals(address)){
                return e.getKey();
            }
        }
        return null;
    }

    private static boolean isMyIpAddress(String ip){
        try {
            return isMyIpAddress(InetAddress.getByName(ip));
        } catch (UnknownHostException e) {
            return false;
        }
    }

    private static boolean isMyIpAddress(InetAddress addr) {
        // Check if the address is a valid special local or loop back
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress())
            return true;

        // Check if the address is defined on any interface
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (SocketException e) {
            return false;
        }
    }
}
