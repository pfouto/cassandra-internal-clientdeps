package org.pfouto.internal.connection;

import io.netty.bootstrap.Bootstrap;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ConnectionManager {

    private static final Logger logger = LogManager.getLogger(ConnectionManager.class);

    private static final int HIGH_PORT = 1805;
    private static final int LOW_PORT = 1806;
    private static final int CLIENT_PORT = 1807;

    private static EventLoopGroup bossGroup;
    private static EventLoopGroup inGroup;
    private static EventLoopGroup outGroup;
    private static final ConcurrentMap<InetAddress, Channel> connections = new ConcurrentHashMap<>();

    public static void init() {
        try {
            bossGroup = new NioEventLoopGroup();
            inGroup = new NioEventLoopGroup();
            outGroup = new NioEventLoopGroup();

            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, inGroup);
            b.channel(NioServerSocketChannel.class);
            b.childHandler(new ChannelInitializer<SocketChannel>() { // (4)
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new MessageDecoder(), new MessageEncoder(), new ConnectionHandler());

                    ch.closeFuture().addListener((ChannelFuture cF) -> {
                        logger.info("Connection from " + cF.channel().remoteAddress() + " closed");
                    });
                }
            });
            b.option(ChannelOption.SO_BACKLOG, 128);
            b.childOption(ChannelOption.SO_KEEPALIVE, true);

            // Bind and start to accept incoming connections.
            ChannelFuture f = b.bind(HIGH_PORT).sync(); // (7)
            logger.info("Server socket opened!");
            f.channel().closeFuture().addListener((ChannelFuture cF) -> {
                logger.error("Server socket closed!");
                System.exit(0);
            });
        } catch (Exception e) {
            logger.error("Exception: " + e.getMessage());
            e.printStackTrace();
            outGroup.shutdownGracefully();
            inGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();

            System.exit(0);
        }
    }

    public static Channel getConnectionTo(InetAddress addr, int port) throws InterruptedException {
        Channel c = connections.get(addr);
        if (c == null) {
            synchronized (connections){
                c = connections.get(addr);
                if(c == null){
                    c = createConnection(addr, port);
                }
            }
        }
        return c;
    }

    public static Channel getConnectionToHigh(InetAddress addr) throws InterruptedException {
        return getConnectionTo(addr, HIGH_PORT);
    }

    public static Channel getConnectionToClient(InetAddress addr) throws InterruptedException {
        return getConnectionTo(addr, CLIENT_PORT);
    }

    public static Channel getConnectionToLow(InetAddress addr) throws InterruptedException {
        return getConnectionTo(addr, LOW_PORT);
    }

    public static ConcurrentMap<InetAddress, Channel> getConnections() {
        return connections;
    }

    private static Channel createConnection(InetAddress addr, int port) throws InterruptedException {
        Bootstrap b = new Bootstrap();
        b.group(outGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);

        b.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) throws Exception {
                ch.pipeline().addLast(new MessageDecoder(), new MessageEncoder(), new ConnectionHandler());
            }
        });
        ChannelFuture f = b.connect(addr, port).sync();
        InetAddress connAddress = ((InetSocketAddress) f.channel().remoteAddress()).getAddress();
        connections.put(connAddress, f.channel());


        f.channel().closeFuture().addListener((ChannelFuture cF) -> {
            logger.info("Connection to " + cF.channel().remoteAddress() + " closed");
            connections.remove(((InetSocketAddress) cF.channel().remoteAddress()).getAddress());
        });
        return f.channel();
    }
}
