package org.pfouto.internal.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class MessageDecoder extends ByteToMessageDecoder { // (1)

    private static final Logger logger = LogManager.getLogger(MessageDecoder.class);

    private int size = -1;

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {

        ByteBufInputStream inputStream = new ByteBufInputStream(in);
        try {

            if (size == -1 && in.readableBytes() >= 4) {
                size = in.readInt();
            }

            if (size != -1 && in.readableBytes() >= size) {
                out.add(Message.decodeMessage(inputStream));
                size = -1;
            }

        } catch (Exception e) {
            StringBuilder sb = new StringBuilder();
            sb.append("Exception decoding message: ").append(" from").append(ctx.channel().remoteAddress()).append(" - ").append(e.getMessage());
            for(int i = 0;i<e.getStackTrace().length; i++){
                sb.append("\n").append(e.getStackTrace()[i].getClassName()).append(" ").append(e.getStackTrace()[i].getLineNumber());
            }
            logger.error(sb.toString());
            System.exit(1);
        }
    }
}
