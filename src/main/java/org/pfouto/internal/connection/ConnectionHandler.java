package org.pfouto.internal.connection;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.state.MessageExecutor;
import org.pfouto.internal.state.LocalMessageReceiver;
import org.pfouto.internal.tree.TreeManager;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;

class ConnectionHandler extends ChannelInboundHandlerAdapter {

    private static final Logger logger = LogManager.getLogger(ConnectionHandler.class);

    private String internalDc;

    @Override
    public void channelRegistered(final ChannelHandlerContext ctx) {

    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws UnknownHostException { // (1)
        internalDc = TreeManager.getInstance().getDcNameByInternalAddress(((InetSocketAddress) ctx.channel().remoteAddress()).getAddress());
        if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
            internalDc = null;
        }

        if (internalDc != null) {
            logger.debug("Channel to internal " + internalDc + " active " + ctx.channel().remoteAddress());
        } else {
            logger.debug("Channel to client " + ctx.channel().remoteAddress() + " active");
        }
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {

        if (internalDc != null && internalDc.equals(TreeManager.getInstance().getLocalDc())) {
            logger.error("Message from myself (!?)");
            logger.error(msg);
            System.exit(0);
        }

        if (internalDc == null || msg instanceof StatusMessage) {
            LocalMessageReceiver.addToDatastoreQueue((Message) msg);
        } else {
            MessageExecutor.addToWaitingMessages((Message) msg);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        logger.error("Exception in channel " + ctx.channel().remoteAddress() + ":\n" + cause);
        ctx.close();
    }

}
