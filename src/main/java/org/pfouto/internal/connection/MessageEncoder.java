package org.pfouto.internal.connection;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.List;
import java.util.Map;

public class MessageEncoder extends ChannelOutboundHandlerAdapter {

    private static final Logger logger = LogManager.getLogger(MessageEncoder.class);

    @Override
    public void write(ChannelHandlerContext ctx, Object o, ChannelPromise promise) {
        if (o instanceof InternalMessage || o instanceof DownMessage || o instanceof ClockMessage
                || o instanceof StatusMessage || o instanceof MigrateMessage) {

            ByteBuf out = ctx.alloc().buffer();
            try {
                ByteBufOutputStream outStream = new ByteBufOutputStream(out);
                //Reserve space for message size
                out.writerIndex(4);
                //Encode message
                Message m = (Message) o;
                m.encode(outStream);
                //Update message size
                int index = out.writerIndex();
                out.writerIndex(0);
                out.writeInt(index - 4);
                out.writerIndex(index);
                ctx.write(out, promise);

            } catch (Exception e) {
                if (out != null && out.refCnt() > 0) out.release(out.refCnt());
                promise.tryFailure(e);
                StringBuilder sb = new StringBuilder();
                sb.append("Exception encoding message: ").append(o).append(" to").append(ctx.channel().remoteAddress()).append(" - ")
                        .append(e.toString()).append(" ").append(e.getMessage());
                for(int i = 0;i<e.getStackTrace().length; i++){
                    sb.append("\n").append(e.getStackTrace()[i].getClassName()).append(" ").append(e.getStackTrace()[i].getLineNumber());
                }
                logger.error(sb.toString());
                e.printStackTrace();
                System.exit(1);
            }

        } else {
            logger.error("Unsupported object received in encoder: " + o);
        }

    }

}