package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;

import java.io.IOException;
import java.net.InetAddress;

public class AckMessage extends Message{

    public final static int CODE = 4;

    private final int id;
    private final InetAddress node;

    public AckMessage(int messageId, InetAddress node, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.id = messageId;
        this.node = node;
    }

    public int getId() {
        return id;
    }

    public InetAddress getNode() {
        return node;
    }

    @Override
    public String toString(){
        return super.toString() + " ID: " + id + " NODE: " + node;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException
    {
        outStream.writeInt(id);
        CompactEndpointSerializationHelper.serialize(node, outStream);
    }

    static AckMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        int id = inputStream.readInt();
        InetAddress node = CompactEndpointSerializationHelper.deserialize(inputStream);
        return new AckMessage(id, node, from, timestamp);
    }

}