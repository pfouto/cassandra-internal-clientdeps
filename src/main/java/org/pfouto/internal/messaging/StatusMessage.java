package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.Pair;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class StatusMessage extends Message {

    public static final int CODE = 5;

    private String dataCenter;
    private Map<String, Pair<Integer, Long>> status;

    public StatusMessage(long timestamp, String dataCenter, Map<String, Pair<Integer, Long>> status) {
        super(null, CODE, timestamp);
        this.status = status;
        this.dataCenter = dataCenter;
    }

    public Map<String, Pair<Integer, Long>> getStatus() {
        return status;
    }

    public String getDataCenter() {
        return dataCenter;
    }

    public String toString() {
        return super.toString() + " dc: " + dataCenter + " status: " + status;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeUTF(dataCenter);
        outStream.writeInt(status.size());
        for (Map.Entry<String, Pair<Integer, Long>> entry : status.entrySet()) {
            outStream.writeUTF(entry.getKey());
            outStream.writeInt(entry.getValue().left);
            outStream.writeLong(entry.getValue().right);
        }
    }

    static StatusMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        String dataCenter = inputStream.readUTF();
        int statusSize = inputStream.readInt();
        Map<String, Pair<Integer, Long>> status = new HashMap<>();
        for(int i = 0; i<statusSize;i++){
            String key = inputStream.readUTF();
            int val = inputStream.readInt();
            long ts = inputStream.readLong();
            status.put(key, Pair.create(val, ts));
        }
        return new StatusMessage(timestamp, dataCenter, status);
    }
}
