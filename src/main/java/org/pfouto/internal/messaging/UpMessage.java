package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class UpMessage extends Message {
    public static final int CODE = 0;

    private final int opId;
    private final Map<String, Integer> clientClock;

    //<DC <messageId, target>>
    private final Map<String, List<Pair<Integer, InetAddress>>> targets;

    public UpMessage(Map<String, List<Pair<Integer, InetAddress>>> targets, int id,
                     Map<String, Integer> clientClock, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.targets = targets;
        this.clientClock = clientClock != null ? clientClock : Collections.emptyMap();
        this.opId = id;
    }

    public int getOpId() {
        return opId;
    }

    public Map<String, Integer> getClientClock() {
        return clientClock;
    }

    public Map<String, List<Pair<Integer, InetAddress>>> getTargets() {
        return targets;
    }

    @Override
    public String toString() {
        return super.toString() + " OP ID: " + opId + " TARGETS: " + targets.entrySet() + " CLOCK: " + clientClock.toString();
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeInt(opId);
        //targets
        outStream.writeInt(targets.size());
        for (Map.Entry<String, List<Pair<Integer, InetAddress>>> entry : targets.entrySet()) {
            outStream.writeUTF(entry.getKey());
            outStream.writeInt(entry.getValue().size());
            for (Pair<Integer, InetAddress> pair : entry.getValue()) {
                outStream.writeInt(pair.left);
                CompactEndpointSerializationHelper.serialize(pair.right, outStream);
            }
        }

        //clientClock
        outStream.writeInt(clientClock.size());
        for (Map.Entry<String, Integer> entry : clientClock.entrySet()) {
            outStream.writeUTF(entry.getKey());
            outStream.writeInt(entry.getValue());
        }
    }

    static UpMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        int opId = inputStream.readInt();
        //targets
        int parameterCount = inputStream.readInt();
        Map<String, List<Pair<Integer, InetAddress>>> targets = new HashMap<>();
        readMap(inputStream, parameterCount, targets);
        //clientClock
        int clientClockSize = inputStream.readInt();
        Map<String, Integer> clientClock = new HashMap<>();
        for(int i = 0; i<clientClockSize;i++){
            String key = inputStream.readUTF();
            int val = inputStream.readInt();
            clientClock.put(key, val);
        }
        //create obj
        return new UpMessage(targets, opId, clientClock, from, timestamp);
    }

    private static void readMap(ByteBufInputStream inputStream, int parameterCount, Map<String, List<Pair<Integer, InetAddress>>> targets) throws IOException {
        if (parameterCount > 0) {
            for (int i = 0; i < parameterCount; i++) {
                String key = inputStream.readUTF();
                int valueSize = inputStream.readInt();
                List<Pair<Integer, InetAddress>> value = new ArrayList<>();
                for (int j = 0; j < valueSize; j++) {
                    int id = inputStream.readInt();
                    InetAddress addr = CompactEndpointSerializationHelper.deserialize(inputStream);
                    value.add(Pair.create(id, addr));
                }
                targets.put(key, value);
            }
        }
    }

}