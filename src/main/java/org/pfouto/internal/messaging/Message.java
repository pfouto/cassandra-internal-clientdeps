package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;

import java.io.IOException;
import java.net.InetAddress;

public abstract class Message {

    private final InetAddress from;
    private final int code;
    private final long timestamp;

    Message(InetAddress from, int code, long timestamp)
    {
        this.from = from;
        this.code = code;
        this.timestamp = timestamp;
    }

    public InetAddress getFrom()
    {
        return from;
    }

    public int getCode() {
        return code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString(){
        return "CODE: " + code + " FROM: " + from;
    }

    public void encode(ByteBufOutputStream outStream) throws IOException {
        outStream.writeInt(code);
        CompactEndpointSerializationHelper.serialize(from, outStream);
        outStream.writeLong(timestamp);
        encodeSpecificParameters(outStream);
    }

    protected abstract void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException;

    public static Message decodeMessage(ByteBufInputStream inputStream) throws Exception {
        int messageCode = inputStream.readInt();
        InetAddress from = CompactEndpointSerializationHelper.deserialize(inputStream);
        long timestamp = inputStream.readLong();

        switch (messageCode){
            case AckMessage.CODE:
                return AckMessage.specificDecodeMessage(inputStream, from, timestamp);
            case ClockMessage.CODE:
                return ClockMessage.specificDecodeMessage(inputStream, from, timestamp);
            case DownMessage.CODE:
                return DownMessage.specificDecodeMessage(inputStream, from, timestamp);
            case InternalMessage.CODE:
                return InternalMessage.specificDecodeMessage(inputStream, from, timestamp);
            case MigrateMessage.CODE:
                return MigrateMessage.specificDecodeMessage(inputStream, from, timestamp);
            case StatusMessage.CODE:
                return StatusMessage.specificDecodeMessage(inputStream, from, timestamp);
            case UpMessage.CODE:
                return UpMessage.specificDecodeMessage(inputStream, from, timestamp);
            default:
                throw new Exception("unknown/unhandled messageType in decoding: " + messageCode);
        }
    }
}
