package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

public class DownMessage extends Message {

    public final static int CODE = 2;

    private final int messageId;
    private final int opId;
    private Map<String, Integer> opClock;

    public DownMessage(int opId, int messageId, Map<String, Integer> opClock, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.messageId = messageId;
        this.opClock = opClock;
        this.opId = opId;
    }

    public Map<String, Integer> getOpClock() {
        return opClock;
    }

    public int getMessageId() {
        return messageId;
    }

    public int getOpId() {
        return opId;
    }

    @Override
    public String toString() {
        return super.toString() + " MSGID: " + messageId + " OPID: " + opId + " CLOCK: " + opClock;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeInt(messageId);
        outStream.writeInt(opId);
        //clock
        outStream.writeInt(opClock.size());
        for (Map.Entry<String, Integer> clockEntry : opClock.entrySet()) {
            outStream.writeUTF(clockEntry.getKey());
            outStream.writeInt(clockEntry.getValue());
        }
    }

    static DownMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        int messageId = inputStream.readInt();
        int opId = inputStream.readInt();
        Map<String, Integer> clock = new HashMap<>();
        int clockSize = inputStream.readInt();
        for (int i = 0; i < clockSize; i++) {
            String key = inputStream.readUTF();
            int val = inputStream.readInt();
            clock.put(key, val);
        }
        return new DownMessage(opId, messageId, clock, from, timestamp);
    }
}