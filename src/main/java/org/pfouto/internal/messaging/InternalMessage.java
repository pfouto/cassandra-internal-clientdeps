package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class InternalMessage extends Message {

    public static final int CODE = 1;

    //<DC <Id, target>>
    private Map<String, List<Pair<Integer, InetAddress>>> targets;
    private Map<String, Integer> depClock;
    private int sourceDcClockVal;
    private String sourceDc;
    private int opId;

    public InternalMessage(Map<String, List<Pair<Integer, InetAddress>>> targets,
                           String sourceDc, int opId, int sourceDcClockVal, Map<String, Integer> depClock,
                           InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.targets = targets;
        this.sourceDc = sourceDc;
        this.opId = opId;
        this.depClock = (depClock != null ? depClock : Collections.emptyMap());
        this.sourceDcClockVal = sourceDcClockVal;
    }

    public InternalMessage filterAndClone(String dc) {
        Map<String, List<Pair<Integer, InetAddress>>> newTargets = new HashMap<>();
        List<Pair<Integer, InetAddress>> pairs = targets.get(dc);
        if (pairs != null) {
            newTargets.put(dc, pairs);
        }
        return new InternalMessage(newTargets, sourceDc, opId, sourceDcClockVal, depClock, getFrom(), getTimestamp());
    }

    public Map<String, List<Pair<Integer, InetAddress>>> getTargets() {
        return targets;
    }

    public Map<String, Integer> getDepClock() {
        return depClock;
    }

    public int getSourceDcClockVal() {
        return sourceDcClockVal;
    }

    public int getOpId() {
        return opId;
    }

    public String getSourceDc() {
        return sourceDc;
    }

    @Override
    public String toString() {
        return super.toString() + " TARGETS: " + targets.entrySet() + " SOURCEDCCLOCKVAL: " + sourceDcClockVal
                + " DEPCLOCK: " + depClock + " SOURCEDC: " + sourceDc + " OPID: " + opId;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeUTF(sourceDc);
        //opId
        outStream.writeInt(opId);
        //Targets
        outStream.writeInt(targets.size());
        for (Map.Entry<String, List<Pair<Integer, InetAddress>>> entry : targets.entrySet()) {
            outStream.writeUTF(entry.getKey());
            outStream.writeInt(entry.getValue().size());
            for (Pair<Integer, InetAddress> pair : entry.getValue()) {
                outStream.writeInt(pair.left);
                CompactEndpointSerializationHelper.serialize(pair.right, outStream);
            }
        }
        //idClock
        outStream.writeInt(sourceDcClockVal);

        //depClock
        outStream.writeInt(depClock.size());
        for (Map.Entry<String, Integer> clockEntry : depClock.entrySet()) {
            outStream.writeUTF(clockEntry.getKey());
            outStream.writeInt(clockEntry.getValue());
        }
    }

    static InternalMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        String sourceDc = inputStream.readUTF();
        int opId = inputStream.readInt();
        int parameterCount = inputStream.readInt();
        Map<String, List<Pair<Integer, InetAddress>>> targets = new HashMap<>();
        readMap(inputStream, parameterCount, targets);
        //idClock
        int sourceDcClockVal = inputStream.readInt();
        //depClock
        int depClockSize = inputStream.readInt();
        Map<String, Integer> depClock = new HashMap<>();
        for(int i = 0;i<depClockSize;i++){
            String key = inputStream.readUTF();
            int val = inputStream.readInt();
            depClock.put(key, val);
        }
        //create obj
        return new InternalMessage(targets, sourceDc, opId, sourceDcClockVal, depClock, from, timestamp);
    }

    private static void readMap(ByteBufInputStream inputStream, int parameterCount, Map<String, List<Pair<Integer, InetAddress>>> targets) throws IOException {
        if (parameterCount > 0) {
            for (int i = 0; i < parameterCount; i++) {
                String key = inputStream.readUTF();
                int valueSize = inputStream.readInt();
                List<Pair<Integer, InetAddress>> value = new ArrayList<>();
                for (int j = 0; j < valueSize; j++) {
                    int id = inputStream.readInt();
                    InetAddress addr = CompactEndpointSerializationHelper.deserialize(inputStream);
                    value.add(Pair.create(id, addr));
                }
                targets.put(key, value);
            }
        }
    }

}