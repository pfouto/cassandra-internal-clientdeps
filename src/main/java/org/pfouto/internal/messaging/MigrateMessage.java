package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

public class MigrateMessage extends Message {

    public final static int CODE = 6;

    private String sourceDc;
    private List<String> possibleDatacenters;
    private Map<String, Integer> clock;
    private long thread;

    public MigrateMessage(long thread, String sourceDc, List<String> possibleDatacenters,
                          Map<String, Integer> clock, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.thread = thread;
        this.sourceDc = sourceDc == null ? "" : sourceDc;
        this.clock = clock == null ? Collections.emptyMap() : clock;
        this.possibleDatacenters = possibleDatacenters;
    }

    public List<String> getPossibleDatacenters() {
        return possibleDatacenters;
    }

    public String getSourceDc() {
        return sourceDc;
    }

    public long getThread() {
        return thread;
    }

    public Map<String, Integer> getClock() {
        return clock;
    }

    @Override
    public String toString() {
        return super.toString() + " THREAD: " + thread + " SOURCEDC: " + sourceDc
                + " DATACENTERS: " + possibleDatacenters + " CLOCK " + clock;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeLong(thread);
        outStream.writeUTF(sourceDc);
        outStream.writeInt(possibleDatacenters.size());
        for (String dc : possibleDatacenters) {
            outStream.writeUTF(dc);
        }
        //Clock
        outStream.writeInt(clock.size());
        for (Map.Entry<String, Integer> clockEntry : clock.entrySet()) {
            outStream.writeUTF(clockEntry.getKey());
            outStream.writeInt(clockEntry.getValue());
        }
    }

    static MigrateMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        long thread = inputStream.readLong();
        String sdc = inputStream.readUTF();
        int listSize = inputStream.readInt();
        List<String> possibleDcs = new ArrayList<>(listSize);
        for(int i = 0;i<listSize;i++){
            possibleDcs.add(inputStream.readUTF());
        }
        //clock
        int cSize = inputStream.readInt();
        Map<String, Integer> c = new HashMap<>();
        for(int i = 0;i<cSize;i++){
            String key = inputStream.readUTF();
            int val = inputStream.readInt();
            c.put(key, val);
        }
        return new MigrateMessage(thread, sdc, possibleDcs, c, from, timestamp);
    }
}