package org.pfouto.internal.messaging;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import org.pfouto.internal.utils.CompactEndpointSerializationHelper;
import org.pfouto.internal.utils.Pair;

import java.io.IOException;
import java.net.InetAddress;

public class ClockMessage extends Message{

    public final static int CODE = 3;

    private Pair<String, Integer> clock;

    public ClockMessage(Pair<String, Integer> clock, InetAddress from, long timestamp) {
        super(from, CODE, timestamp);
        this.clock = clock;
    }

    public Pair<String, Integer> getClock() {
        return clock;
    }

    public static ClockMessage fromInternal(InternalMessage m){
        String sourceDc = m.getSourceDc();
        Pair<String, Integer> pair = Pair.create(sourceDc, m.getSourceDcClockVal());
        return new ClockMessage(pair, m.getFrom(), m.getTimestamp());
    }

    @Override
    public String toString(){
        return super.toString() + " CLOCK: " + clock;
    }

    @Override
    protected void encodeSpecificParameters(ByteBufOutputStream outStream) throws IOException {
        outStream.writeUTF(clock.left);
        outStream.writeInt(clock.right);
    }

    static ClockMessage specificDecodeMessage(ByteBufInputStream inputStream, InetAddress from, long timestamp) throws IOException {
        String left = inputStream.readUTF();
        int right = inputStream.readInt();
        return new ClockMessage(Pair.create(left, right), from, timestamp);
    }

}