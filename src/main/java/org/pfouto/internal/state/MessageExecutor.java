package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.main.Main;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.*;

public class MessageExecutor implements Runnable {

    private static final Logger logger = LogManager.getLogger(MessageExecutor.class);

    static final Map<String, BlockingQueue<Message>> waitingMutations = new ConcurrentHashMap<>();
    static final Queue<MigrateMessage> waitingMigrations = new ConcurrentLinkedQueue<>();
    static Map<Pair<InetAddress, Integer>, WaitingState> currentMutations = new ConcurrentHashMap<>();

    private boolean migrateQueue;
    private String dataCenter;

    public MessageExecutor(String dataCenter, boolean migrateQueue) {
        this.migrateQueue = migrateQueue;
        this.dataCenter = dataCenter;
    }

    @Override
    public void run() {
        try {
            logger.info("Starting thread: " + Thread.currentThread().getName());
            if (migrateQueue) {
                loopMigrationQueue();
            } else {
                boolean localDc = TreeManager.getInstance().getLocalDc().equals(dataCenter);
                if (localDc) {
                    loopLocalMutationQueue(waitingMutations.get(dataCenter));
                } else {
                    loopRemoteMutationQueue(waitingMutations.get(dataCenter));
                }
            }
            assert false;
        } catch (Exception e) {
            logger.error("Exception in executor thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }

    //private static final int SLEEP_MS_MIGRATION = 100;

    private void loopMigrationQueue() throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {
            while (true) {
                MigrateMessage peek = waitingMigrations.peek();
                if (peek != null && canExecuteMessage(peek)) {
                    waitingMigrations.remove();
                    executeMigrateMessage(peek);
                    LocalMessageReceiver.internalMessagesExecuted++;
                } else {
                    break;
                }
            }
            Thread.sleep(Main.REMOTE_WAIT_MS);
        }
    }

    private void loopRemoteMutationQueue(BlockingQueue<Message> workingQueue) throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {
            while (true) {
                Message peek = workingQueue.peek();
                if (peek != null && canExecuteMessage(peek)) {
                    workingQueue.remove();
                    executeMutateMessage(peek);
                    LocalMessageReceiver.internalMessagesExecuted++;
                } else {
                    break;
                }
            }
            Thread.sleep(Main.REMOTE_WAIT_MS);
        }
    }

    private void loopLocalMutationQueue(BlockingQueue<Message> workingQueue) throws InterruptedException {
        //noinspection InfiniteLoopStatement
        while (true) {
            Message take = workingQueue.take();
            assert  canExecuteMessage(take);
            executeMutateMessage(take);
            LocalMessageReceiver.internalMessagesExecuted++;
        }
    }

    private boolean canExecuteMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            return checkMessageDependencies(m.getDepClock());
        } else if (msg.getCode() == ClockMessage.CODE) {
            return true;
        } else if (msg.getCode() == MigrateMessage.CODE) {
            MigrateMessage m = (MigrateMessage) msg;
            return checkMessageDependencies(m.getClock());
        } else {
            logger.error("Unknown message received 'canExecute': " + msg);
            System.exit(1);
            return false;
        }
    }

    static boolean checkMessageDependencies(Map<String, Integer> messageClock) {
        for (Map.Entry<String, Integer> e : messageClock.entrySet()) {
            String dc = e.getKey();
            int opClock = e.getValue();
            int myClock = LocalMessageReceiver.executedClock.getOrDefault(dc, 0);

            if (opClock > myClock) {
                return false;
            }
        }
        return true;
    }

    private void executeMutateMessage(Message msg) {
        if (msg.getCode() == InternalMessage.CODE) {
            InternalMessage m = (InternalMessage) msg;
            logger.debug("Executing internal message " + m);
            executeInternalMessage(m);
        } else if (msg.getCode() == ClockMessage.CODE) {
            ClockMessage m = (ClockMessage) msg;
            logger.debug("Executing clock message " + m);
            executeClockMessage(m);
        } else {
            logger.error("Unknown message received in executor: " + msg);
        }
    }

    private void executeMigrateMessage(MigrateMessage msg) {
        logger.debug("Executing migrate message " + msg);

        TreeManager tree = TreeManager.getInstance();
        assert msg.getPossibleDatacenters().get(0).equals(tree.getLocalDc());
        logger.debug("Redirecting migrate to client: " + msg.getFrom());
        try {
            ConnectionManager.getConnectionToClient(msg.getFrom()).writeAndFlush(msg);
        } catch (Exception e) {
            logger.error("Failed to migration message to client " + msg.getFrom() + ". " + e.getMessage());
        }
    }

    private void executeClockMessage(ClockMessage msg) {
        Pair<String, Integer> clock = msg.getClock();
        LocalMessageReceiver.incExecutedClock(clock.left, clock.right);
    }

    private void executeInternalMessage(InternalMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();
        if (msg.getTargets().containsKey(localDc)) {

            WaitingState waitingState = new WaitingState(msg, localDc);
            for (Pair<InetAddress, Integer> key : waitingState.getMapKeys()) {
                WaitingState put = currentMutations.put(key, waitingState);
                assert put == null;
            }
            //DEBUG
            LocalMessageReceiver.currentMutationSize = currentMutations.size();
            if (LocalMessageReceiver.currentMutationSize > LocalMessageReceiver.maxCurrentMutationSize) {
                LocalMessageReceiver.maxCurrentMutationSize = LocalMessageReceiver.currentMutationSize;
            }

            sendToLocalNodes(msg);
        } else {
            logger.warn("Remote Write!");
            executeClockMessage(ClockMessage.fromInternal(msg));
            sendToLocalNodes(msg); //just to send the id down in order to complete write, should not happen in local writes
        }
    }

    private void sendToLocalNodes(InternalMessage msg) {
        logger.debug("Sending to local nodes:" + msg.getSourceDcClockVal());

        List<Pair<Integer, InetAddress>> localNodes = msg.getTargets().get(TreeManager.getInstance().getLocalDc());

        boolean localDc = TreeManager.getInstance().getLocalDc().equals(msg.getSourceDc());
        boolean sentToCoordinator = !localDc; //if not localDc, set this to true (since its not needed)

        HashMap<String, Integer> newDepClock = new HashMap<>(msg.getDepClock());
        newDepClock.put(msg.getSourceDc(), msg.getSourceDcClockVal());

        if (Main.NO_DEPS_TO_DISK) {
            newDepClock.clear();
            newDepClock.put(msg.getSourceDc(), 0);
        }

        if (localNodes != null) {
            for (Pair<Integer, InetAddress> p : localNodes) {
                DownMessage down;
                if (!sentToCoordinator && p.right.equals(msg.getFrom())) {
                    sentToCoordinator = true;
                    down = new DownMessage(msg.getOpId(), p.left, newDepClock, msg.getFrom(), msg.getTimestamp());
                } else {
                    down = new DownMessage(-1, p.left, newDepClock, msg.getFrom(), msg.getTimestamp());
                }
                try {
                    logger.debug("Sending to: " + p.right + " - " + down);
                    ConnectionManager.getConnectionToLow(p.right).writeAndFlush(down);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + p.right + ". " + e.getMessage());
                }
            }
        }
        if (!sentToCoordinator) {
            DownMessage down = new DownMessage(msg.getOpId(), -1, newDepClock, msg.getFrom(), msg.getTimestamp());
            try {
                logger.debug("Sending to: " + msg.getFrom() + " - " + down);
                ConnectionManager.getConnectionToLow(msg.getFrom()).writeAndFlush(down);
            } catch (Exception e) {
                logger.error("Failed to forward message to coordinator " + msg.getFrom() + ". " + e.getMessage());
            }
        }
    }

    static int getWaitingMessagesSize() {
        return waitingMutations.values().stream().mapToInt(Queue::size).sum();
    }

    public static void addToWaitingMessages(Message msg) {
        logger.debug("New message: " + msg);

        if (msg.getCode() == MigrateMessage.CODE) {
            waitingMigrations.add((MigrateMessage) msg);
        } else {
            String sourceDc;
            if (msg.getCode() == InternalMessage.CODE) {
                sourceDc = ((InternalMessage) msg).getSourceDc();
            } else if (msg.getCode() == ClockMessage.CODE) {
                sourceDc = ((ClockMessage) msg).getClock().left;
            } else {
                logger.error("Unknown message received in addToWaitingMessages: " + msg);
                System.exit(1);
                return;
            }

            BlockingQueue<Message> messages = waitingMutations.get(sourceDc);

            if (messages == null) {
                messages = waitingMutations.putIfAbsent(sourceDc, new LinkedBlockingQueue<>());
                if (messages == null) {
                    MessageExecutor newExecutor = new MessageExecutor(sourceDc, false);
                    new Thread(newExecutor, sourceDc + "Executor").start();
                    messages = waitingMutations.get(sourceDc);
                }
            }
            messages.add(msg);
        }
    }
}