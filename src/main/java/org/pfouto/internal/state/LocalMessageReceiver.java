package org.pfouto.internal.state;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pfouto.internal.connection.ConnectionManager;
import org.pfouto.internal.messaging.*;
import org.pfouto.internal.tree.TreeManager;
import org.pfouto.internal.utils.Pair;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.*;

public class LocalMessageReceiver implements Runnable {

    private static final Logger logger = LogManager.getLogger(LocalMessageReceiver.class);

    private static BlockingQueue<Message> datastoreMessages = new LinkedBlockingQueue<>();

    private static LinkedList<UpMessage> waitingUpMessages = new LinkedList<>();
    private long lastWaitingCheck = 0;
    private static final long WAITING_CHECK_INTERVAL = 200;

    private static int opCounter = 0;

    private static Map<String, Queue<Integer>> aheadExecutedClocks = new ConcurrentHashMap<>();
    static final Map<String, Integer> executedClock = new ConcurrentHashMap<>();

    private long lastStatus = 0;
    private static final long STATUS_INTERVAL = 3000;

    private Map<String, Pair<Integer, Long>> internalsStatus = new HashMap<>();

    //debug
    private static int localMessagesHandled = 0;
    private static int totalLocalMessagesHandled = 0;

    private static int mutationsCompleted = 0;
    private static int mutationsCompletedTotal = 0;

    static int currentMutationSize = 0;
    static int maxCurrentMutationSize = 0;


    static int internalMessagesExecuted = 0;
    private static int totalIntExec = 0;

    public LocalMessageReceiver() {
    }

    private void mainLoop() throws InterruptedException {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (Map.Entry<String, BlockingQueue<Message>> e : MessageExecutor.waitingMutations.entrySet()) {
                String k = e.getKey();
                Queue<Message> v = e.getValue();
                logger.info(k + ":" + v.size());
                int messages = 5;
                while (!v.isEmpty() && messages > 0) {
                    messages--;
                    Message msg = v.poll();
                    logger.info(msg);
                }
            }

            logger.info("Waiting muts: " + MessageExecutor.currentMutations.size());
            int messages = 5;
            for (Map.Entry<Pair<InetAddress, Integer>, WaitingState> e : MessageExecutor.currentMutations.entrySet()) {
                logger.info(e.getKey() + " " + e.getValue().getMessage() + " " + e.getValue().getNodesMap() + " " + e.getValue().isCompleted());
                messages--;
                if(messages <0)
                    return;
            }
            LogManager.shutdown();
        }));

        //noinspection InfiniteLoopStatement
        while (true) {

            //Normal queue check
            Message in = datastoreMessages.poll(100, TimeUnit.MILLISECONDS);
            if (in != null) {
                if (in.getCode() == UpMessage.CODE && !MessageExecutor.checkMessageDependencies(((UpMessage) in).getClientClock()))
                {
                    waitingUpMessages.add((UpMessage) in);
                } else {
                    handleDatastoreMessage(in);
                }
            }

            long now = System.currentTimeMillis();
            //Cycle all waiting up messages
            if(now - lastWaitingCheck > WAITING_CHECK_INTERVAL){
                Iterator<UpMessage> iterator = waitingUpMessages.iterator();
                while (iterator.hasNext()) {
                    UpMessage m = iterator.next();
                    if (MessageExecutor.checkMessageDependencies(m.getClientClock())) {
                        iterator.remove();
                        handleDatastoreMessage(m);
                    }
                }
                lastWaitingCheck = now;
            }

            //Send status
            if (now - lastStatus > STATUS_INTERVAL) {
                int recQueue = datastoreMessages.size() + waitingUpMessages.size();
                int execQueue = MessageExecutor.getWaitingMessagesSize();
                //propagate info
                propagateSaturationState(recQueue + execQueue);
                outputLogs(recQueue, execQueue);
                lastStatus = now;
            }
        }
    }

    private void outputLogs(int recQueue, int execQueue) {
        totalIntExec += internalMessagesExecuted;
        totalLocalMessagesHandled += localMessagesHandled;
        mutationsCompletedTotal += mutationsCompleted;

        //LOG stuff
        StringBuilder sb = new StringBuilder();

        sb.append("\nRecQ: ").append(recQueue)
                .append("\tWaitingUpQ: ").append(waitingUpMessages.size())
                .append("\tExeQ: ").append(execQueue)
                .append("\tnMut: ").append(currentMutationSize).append("(").append(maxCurrentMutationSize).append(")");

        sb.append("\n")
                .append("RecHandled: ").append(localMessagesHandled).append("(").append(totalLocalMessagesHandled).append(")")
                .append("\tExecHandled: ").append(internalMessagesExecuted).append("(").append(totalIntExec).append(")")
                .append("\tMutComp: ").append(mutationsCompleted).append("(").append(mutationsCompletedTotal).append(")");

        sb.append("\n")
                .append("Clock: ").append(executedClock.toString());

        sb.append("\n");
        sb.append("Migrate: ").append(MessageExecutor.waitingMigrations.size()).append("\t");
        MessageExecutor.waitingMutations.forEach((k, v) ->
                sb.append(k).append(": ").append(v.size()).append("\t"));

        if (recQueue > 100000 || execQueue > 100000) {
            logger.error(sb.toString());
        } else if (recQueue > 20000 || execQueue > 20000) {
            logger.warn(sb.toString());
        } else {
            logger.info(sb.toString());
        }

        localMessagesHandled = 0;
        mutationsCompleted = 0;
        internalMessagesExecuted = 0;
    }

    private void handleDatastoreMessage(Message msg) {
        localMessagesHandled++;

        if (msg.getCode() != StatusMessage.CODE) {
            logger.debug("Message Received: " + msg);
        }

        if (msg.getCode() == UpMessage.CODE) {
            handleUpMessage((UpMessage) msg);
        } else if (msg.getCode() == AckMessage.CODE) {
            handleAckMessage((AckMessage) msg);
        } else if (msg.getCode() == StatusMessage.CODE) {
            handleStatusMessage((StatusMessage) msg);
        } else if (msg.getCode() == MigrateMessage.CODE) {
            handleMigrateMessage((MigrateMessage) msg);
        } else {
            logger.error("Unknown message received in receiver: " + msg);
        }
    }

    private void handleUpMessage(UpMessage msg) {
        String localDc = TreeManager.getInstance().getLocalDc();
        int sourceDcClockVal = ++opCounter;
        InternalMessage internalMessage =
                new InternalMessage(msg.getTargets(), localDc, msg.getOpId(), sourceDcClockVal, msg.getClientClock(), msg.getFrom(), msg.getTimestamp());
        propagateInternalMessage(internalMessage);
        MessageExecutor.addToWaitingMessages(internalMessage);
    }

    private void handleMigrateMessage(MigrateMessage m) {
        TreeManager tree = TreeManager.getInstance();
        assert m.getSourceDc().equals(tree.getLocalDc());

        String bestDc = null;
        int bestDcLoad = Integer.MAX_VALUE;
        for (String dc : m.getPossibleDatacenters()) {
            Pair<Integer, Long> pair = internalsStatus.get(dc);
            if (pair == null)
                continue;
            int load = pair.left;
            if (load < bestDcLoad) {
                bestDcLoad = load;
                bestDc = dc;
            }
        }
        assert bestDc != null;

        m.getPossibleDatacenters().clear();
        m.getPossibleDatacenters().add(bestDc);
        assert m.getPossibleDatacenters().size() == 1;

        //propagate
        try {
            InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(bestDc));
            ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
        } catch (Exception e) {
            logger.error("Failed to migration message to " + bestDc + ". " + e.getMessage());
        }
    }

    private void handleStatusMessage(StatusMessage sm) {
        for (Map.Entry<String, Pair<Integer, Long>> entry : sm.getStatus().entrySet()) {
            String dc = entry.getKey();
            Pair<Integer, Long> remotePair = entry.getValue();
            Pair<Integer, Long> localPair = internalsStatus.get(dc);
            if (localPair == null || remotePair.right > localPair.right) {
                internalsStatus.put(dc, remotePair);
            }
        }
    }

    private void handleAckMessage(AckMessage msg) {
        Pair p = Pair.create(msg.getFrom(), msg.getId());
        WaitingState waitingState = MessageExecutor.currentMutations.remove(p);
        if (waitingState != null) {
            waitingState.receiveAck(msg.getNode(), msg.getId());
            if (waitingState.isCompleted()) {
                String sourceDc = waitingState.getSourceDc();
                logger.debug("Mutation completed " + sourceDc + " " + waitingState.getMessage().getSourceDcClockVal());
                incExecutedClock(sourceDc, waitingState.getMessage().getSourceDcClockVal());
                mutationsCompleted++;
            }
        }

        //DEBUG
        currentMutationSize = MessageExecutor.currentMutations.size();
        if (currentMutationSize > maxCurrentMutationSize) {
            maxCurrentMutationSize = currentMutationSize;
        }

    }

    private void propagateInternalMessage(InternalMessage msg) {
        TreeManager tree = TreeManager.getInstance();
        ClockMessage cm = null;

        assert msg.getSourceDc().equals(tree.getLocalDc());
        for (String dc : tree.getDownBranches(msg.getSourceDc())) {

            //Decide message or clock and create
            Message m;
            if (msg.getTargets().get(dc) != null) {
                m = msg.filterAndClone(dc);
                logger.debug("Redirecting to: " + dc);
            } else {
                if (cm == null) cm = ClockMessage.fromInternal(msg);
                logger.debug("Clock to: " + dc + " " + cm);
                m = cm;
            }

            //Send
            if (m != null) {
                try {
                    InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                    ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(m);
                } catch (Exception e) {
                    logger.error("Failed to forward message to " + dc + ". " + e.getMessage());
                }
            }
        }
    }

    static synchronized void incExecutedClock(String sourceDc, int clockVal) {
        int oldClock = executedClock.getOrDefault(sourceDc, 0);
        Queue<Integer> aheadClocks = aheadExecutedClocks.computeIfAbsent(sourceDc, k -> new PriorityBlockingQueue<>());
        if (oldClock == clockVal - 1) {
            int currentClock = clockVal;
            while (!aheadClocks.isEmpty() && aheadClocks.peek() == currentClock + 1) {
                currentClock = aheadClocks.remove();
            }
            executedClock.put(sourceDc, currentClock);
            logger.debug("New clock: " + executedClock);
        } else {
            assert clockVal > oldClock + 1;
            aheadClocks.add(clockVal);
        }
    }

    private void propagateSaturationState(int queuesSize) {
        TreeManager tree = TreeManager.getInstance();

        long time = System.currentTimeMillis();
        internalsStatus.put(tree.getLocalDc(), Pair.create(queuesSize, time));
        StatusMessage sm = new StatusMessage(time, tree.getLocalDc(), new HashMap<>(internalsStatus));

        List<String> targets = new LinkedList<>();
        targets.addAll(tree.getDownBranches(tree.getMainTree()));
        String inverse = tree.getUpBranch(tree.getMainTree());
        if (inverse != null)
            targets.add(inverse);

        for (String dc : targets) {
            try {
                InetAddress dcAddress = InetAddress.getByName(tree.getDcAddress(dc));
                ConnectionManager.getConnectionToHigh(dcAddress).writeAndFlush(sm);
            } catch (Exception e) {
                logger.error("Failed to send status message to " + dc + ". " + e.getMessage());
            }
        }
    }

    public static void addToDatastoreQueue(Message msg) {
        datastoreMessages.add(msg);
    }

    @Override
    public void run() {
        try {
            logger.info("Starting thread: " + Thread.currentThread().getName());
            mainLoop();
        } catch (Exception e) {
            logger.error("Exception in receiver thread: " + e);
            e.printStackTrace();
            System.exit(1);
        }
    }
}
